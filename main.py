from sanic import Sanic
from Users.user import Users


app = Sanic()
users = Users(app)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
