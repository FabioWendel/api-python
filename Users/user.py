from mysql import connector
from sanic.response import json as response_json
from sanic import Sanic


class Users(object):
    def __init__(self, app: Sanic):
        mydb = {'user': 'root', 'password': '', 'host': 'localhost', 'database': 'projectDB', 'raise_on_warnings': True}
        self.cnx = connector.connect(**mydb)
        self.cursor = self.cnx.cursor()
        app.add_route(self.users, "/users", methods=["GET", "POST"])
        app.add_route(self.user, "/user/<id_user>", methods=["GET", "PUT", "UPDATE", "DELETE"])

    async def users(self, request):
        print("Metodos ===>>", request.method)
        if request.method == 'GET':

            query = "SELECT * FROM tbl_user"
            self.cursor.execute(query)
            users = list()
            for (user_id, user_name, user_email, password) in self.cursor.fetchall():
                user = dict()
                user["id"] = user_id
                user["name"] = user_name
                user["email"] = user_email
                users.append(user)
            return response_json(users)

        elif request.method == 'POST':

            fields = list()
            for key, val in request.json.items():
                fields.append(val)
            query = "INSERT INTO tbl_user ( user_name, user_email, user_password) VALUES ( %s, %s, %s)"
            self.cursor.execute(query, fields)
            self.cnx.commit()
            return response_json(request.json)

    async def user(self, request, id_user):
        if request.method == 'DELETE':
            query = "DELETE FROM tbl_user WHERE user_id = {}".format(id_user)
            print(query)

            self.cursor.execute(query)
            self.cnx.commit()

            return response_json(request.json)

        elif request.method == 'PUT':
            fields = list()
            for key, val in request.json.items():
                fields.append(val)

            query = "UPDATE tbl_user SET user_name = %s, user_email = %s, user_password = %s WHERE user_id = %s"
            fields.append(id_user)

            self.cursor.execute(query, fields)
            self.cnx.commit()
            return response_json(request.json)

        elif request.method == 'GET':

            query = "SELECT * FROM tbl_user WHERE user_id = {}".format(id_user)
            self.cursor.execute(query)
            users = list()
            for (user_id, user_name, user_email, password) in self.cursor.fetchall():
                user = dict()
                user["id"] = user_id
                user["name"] = user_name
                user["email"] = user_email
                users.append(user)
            return response_json(users)
